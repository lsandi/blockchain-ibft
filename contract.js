const haversine = require('haversine')


function WaybillContract(data, id) {
  this.id = id;
  this.order_id = data.order_id;
  this.truck_id = data.truck_id;
  this.driver_id = data.driver_id;
  this.sender_id = data.sender_id;
  this.receiver_id = data.receiver_id;
  this.date_send = data.date_send;

  return this;
}

WaybillContract.prototype.calculatePayment = function(start, end, numItem) {
  let distance = haversine(start, end);
  let paymentAmount = distance * numItem * 500;

  return paymentAmount;
}


module.exports = WaybillContract;