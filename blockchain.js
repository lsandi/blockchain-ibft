const sha256 = require('sha256');
const short = require('short-uuid');

function Blockchain(data, id) {
  this.id = id || short.generate();
  this.chain = [];
  this.buffer = {}; //for blocks to vote
  this.votingBuffer = {}; //votes for unreceived block
  this.status = '' //state of the blockchain
  const genesisBlock = this.createBlock("0", {
    data: data,
  })
  this.chain.push(genesisBlock);
}

Blockchain.prototype.updateInstance = function(blockchain) {
  this.id = blockchain.id
  this.chain = blockchain.chain
  this.buffer = blockchain.buffer //for blocks to vote
  this.votingBuffer = blockchain.votingBuffer //votes for unreceived block
  this.status = blockchain.status
}

Blockchain.prototype.getLastBlock = function() {
  return this.chain[this.chain.length -1];
}

Blockchain.prototype.getLasts = function(range) {
  if (this.chain.length < range) range = this.chain.length;
  const start = this.chain.length - range;
  const end = this.chain.length;

  return this.chain.slice(start, end);
}

Blockchain.prototype.getBlockHash = function(previousBlockHash, data) {
  const dataAsString = previousBlockHash + JSON.stringify(data);
  return sha256(dataAsString);
}

Blockchain.prototype.createBlock = function(lastBlockHash, data, timestamp) {
  let index = 0;
  try {
    index = this.chain[this.chain.length -1].index + 1;
  } catch (err) {
    index = 0;
  }

  return {
    index: index,
    timestamp: timestamp,
    data: data,
    hash: this.getBlockHash(lastBlockHash, data),
    previousBlockHash: lastBlockHash,
  };
}

Blockchain.prototype.logBuffer = function() {
  console.log(`Current buffer size: ${Object.keys(this.buffer).length}`);
  console.log(`Current voting buffer size: ${Object.keys(this.votingBuffer).length}`);

}

Blockchain.prototype.putBlockOnHold = function(block) {
  if (!(block['hash'] in this.buffer)) {
    this.buffer[block['hash']] = {
      block: block,
      voting: {
        nodesVoted: [],
        votes: [],
        yesVotes: 0,
        nodesCommiting: [],
        commitMessages: 0
      }
    }

    if (block['hash'] in this.votingBuffer) {
      this.votingBuffer[block['hash']].forEach(voteInfo => {
        this.processVote(voteInfo.blockHash,
                         voteInfo.blockIndex,
                         voteInfo.nodeAddress,
                         voteInfo.vote);
      });
    }
  } else {
    console.log(`Block ${block['hash']} already in buffer`);
  }

  this.logBuffer();
}

Blockchain.prototype.isBlockInBlockchain = function(hash, index) {
  return (this.chain[index-1]['hash'] === hash);
}

Blockchain.prototype.holdVoteOnBuffer = function(blockHash, voteInfo) {
  if (!(blockHash in this.votingBuffer)) {
    this.votingBuffer[blockHash] = [];
  }

  this.votingBuffer[blockHash].push(voteInfo);
}

//return number of yes vote
Blockchain.prototype.processVote = function(blockHash, blockIndex, nodeAddress, vote) {
  //vote come after consensus
  if (this.isBlockInBlockchain(blockHash, blockIndex)) {
    console.log(`Block ${blockHash} already accepted. Index: ${blockIndex}`);
    return -1;
  }

  //check block being voted in buffer
  if (blockHash in this.buffer) {
    if (this.buffer[blockHash].voting.nodesVoted.indexOf(nodeAddress) == -1) {
      this.buffer[blockHash].voting.nodesVoted.push(nodeAddress);
      this.buffer[blockHash].voting.votes.push({
        node: nodeAddress,
        vote: vote
      });
      if (vote === "yes") this.buffer[blockHash].voting.yesVotes++;
    }
  } else {
    // vote received before block in buffer
    this.holdVoteOnBuffer(blockHash, {
      blockHash: blockHash,
      blockIndex: blockIndex,
      nodeAddress: nodeAddress,
      vote: vote
    });
    return { warning: `block ${blockHash} being voted not yet received`}
  }
  return {
    totalVotes: this.buffer[blockHash].voting.nodesVoted.length,
    yesVotes: this.buffer[blockHash].voting.yesVotes
  }

}

Blockchain.prototype.getVotes = function(blockHash, blockIndex) {
  if (this.isBlockInBlockchain(blockHash, blockIndex)) {
    console.log(`Block ${blockHash} already accepted. Index: ${blockIndex}`);
    return -1;
  }
  if (!(blockHash in this.buffer)) {
    return { warning: `block ${blockHash} being voted not yet received`}
  }
  return {
    totalVotes: this.buffer[blockHash].voting.nodesVoted.length,
    yesVotes: this.buffer[blockHash].voting.yesVotes
  }
}

Blockchain.prototype.processCommit = function(blockHash, blockIndex, nodeAddress) {
  //vote come after consensus
  if (this.isBlockInBlockchain(blockHash, blockIndex)) {
    console.log(`Block ${blockHash} already accepted. Index: ${blockIndex}`);
    return -1;
  }

  //check block being commited in buffer
  if (blockHash in this.buffer) {
    this.buffer[blockHash].voting.nodesCommiting.push(nodeAddress);
    this.buffer[blockHash].voting.commitMessages++;
    
  } else {
    // commit received before block in buffer
    return { warning: `block ${blockHash} being commited not yet received`}
  }
  return {
    totalCommiting: this.buffer[blockHash].voting.nodesCommiting.length,
    commitMessages: this.buffer[blockHash].voting.commitMessages
  }

}

Blockchain.prototype.getCommits = function(blockHash, blockIndex) {
  if (this.isBlockInBlockchain(blockHash, blockIndex)) {
    console.log(`Block ${blockHash} already accepted. Index: ${blockIndex}`);
    return -1;
  }
  if (!(blockHash in this.buffer)) {
    return { warning: `block ${blockHash} being commited not yet received`}
  }
  return {
    totalCommiting: this.buffer[blockHash].voting.nodesCommiting.length,
    commitMessages: this.buffer[blockHash].voting.commitMessages
  }
}

Blockchain.prototype.closeVotingOnBlock = function(hash) {
  delete this.buffer[hash];
  delete this.votingBuffer[hash];

  this.logBuffer();
}

Blockchain.prototype.addBlockOnBuffer = function(hash) {
  if (!(hash in this.buffer)) {
    throw `Trying to add block that is not on buffer: ${hash}`;
  }

  this.chain.push(this.buffer[hash].block);
  this.closeVotingOnBlock(hash);
}

Blockchain.prototype.isValidNewBlock = function(newBlock) {
  const lastBlock = this.getLastBlock();
  const correctIndex = newBlock['index'] === lastBlock['index'] + 1;
  const correctLastHash = newBlock['previousBlockHash'] === lastBlock['hash'];
  const recalculatedNewHash = this.getBlockHash(newBlock['previousBlockHash'], newBlock['data']);
  const correctNewHash = recalculatedNewHash === newBlock['hash'];

  return {
    details: {
      "correctIndex": correctIndex,
      "correctLastHash": correctLastHash,
      "correctNewHash": correctNewHash,
    },
    isValid: (correctIndex && correctLastHash && correctNewHash)
  };
}

module.exports = Blockchain