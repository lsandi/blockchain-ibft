const Blockchain = require ('./blockchain');
const WaybillContract = require('./contract');
const VotingStatistics = require('./votingStatistics');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const rp = require('request-promise');
const request = require('request');
const short = require('short-uuid');
require('dotenv').config();
const nodeIp = process.env.HOST;
const nodeUuid = short.generate();
const PORT = process.env.PORT;
const runningSince = (new Date()).toISOString().replace("T", " ").replace(/\.\d+.*/, "");
const mysql = require('mysql2/promise');
const jsonSql = require('json-sql')({separatedValues: false, wrappedIdentifiers: false});
const db = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  multipleStatements: true
})
  
let listBlockchain = {};
let isBlockchainAvailable = true;
let isCreateBlockSuccess = false;
let networkNodes = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Terdapat error')
})

const log = _str => {
  if (typeof _str === 'object') {
    _str = JSON.stringify(_str);
  }
  console.log(`[${getCurrentTimestamp()}]: ${_str}`);
}

let votingStatistics = null;

function isEndpointEnabled(req, res, callback) {
  isBlockchainAvailable ? callback() : res.json({ note: "this endpoint isn't available"})
}

// function isValid(ip) {
//   return !!ip;
// }

function getURI(ip, route) {
  return `http://${ip}${route}`;
}

function makePostRequest(ip, route, bodyJSON) {
  return {
    uri: getURI(ip, route),
    method: 'POST',
    body: bodyJSON,
    json: true,
  };
}

app.get('/', function (req, res) {
  isEndpointEnabled(req, res, () => {
    res.json({
      note: `Node running on address: ${nodeIp}`,
      "nodeId": nodeUuid,
      "runningSince": runningSince,
      "networkNodes": networkNodes
    });
  
});

});

app.post('/newblockchain', function (req, res){
  let data = req.body.data;
  const id = req.body.id || short.generate();
  if (req.body.hasOwnProperty('initContract')){
    let waybillData = req.body.waybillData
    let newwaybill = new WaybillContract(waybillData, id);
    let start = {
      latitude: waybillData.origin_latitude,
      longitude: waybillData.origin_longitude,
    }
    let end = {
      latitude: waybillData.destination_latitude,
      longitude: waybillData.destination_longitude,
    }
    let paymentAmount = newwaybill.calculatePayment(start,end,waybillData.numItem);
    data.paymentAmount = paymentAmount;
  }
  let newblockchain = new Blockchain(data, id);
  let isBroadcast = true;
  if (id === 'database') {
    if (!(listBlockchain.hasOwnProperty('database'))) {
      listBlockchain[newblockchain['id']] = newblockchain;
      log(`New database blockchain with genesis block ${req.body.data}`)
      const blockquery = listBlockchain['database'].chain.find(block => block.index ==0).data.data;
      db.query(blockquery, function(error,results, fields) {
        if (error){
          console.log("ERROR", error.sqlMessage)
          delete listBlockchain['database'];
        };
        console.log('query result: ', results)
      });
    } else {
      isBroadcast = false;
    }
  } else {
    listBlockchain[newblockchain['id']] = newblockchain;
    log(`New blockchain with genesis block ${req.body.data}`)
  }

  if (isBroadcast) {
    if (networkNodes.length > 0) {
      const newBlockchainPromises = [];
      for (let i = 0; i < networkNodes.length; i++) {
        newBlockchainPromises.push(rp(makeNewBlockchainRequest(networkNodes[i], {data: newblockchain})));
      }
  
      Promise.all(newBlockchainPromises)
      .then(function(body) {
        log(`New blockchain transmission success:\n${JSON.stringify(body)}`);
        res.end();
      })
      .catch(err => {
        log(`error on sending new blockchain request: ${err}`);
      });
    }
    res.json({
      id: newblockchain.id,
      chain: newblockchain.chain
    })
  } else {
    res.json({
      err: "database blockchain already there"
    })
  }
})

app.post('/copyblockchain', function (req, res){
  log(`Received new blockchain with id ${req.body.data.id}`)
  const data = req.body.data;
  listBlockchain[data['id']] = data;
  res.json({
    "id": data.id
  })
})

function makeNewBlockchainRequest(networkNodeUrl, data) {
  return makePostRequest(networkNodeUrl, `/copyblockchain`, data);
}

app.get('/listblockchain', function (req, res) {
  isEndpointEnabled(req, res, () => res.send(listBlockchain));
});

app.get('/blockchain/:id', function (req, res) {
  const id = req.params.id
  isEndpointEnabled(req, res, () => res.send(listBlockchain[id]));
});

app.get('/blockchain/size/:id', (req, res) => {
  const id = req.params.id
  isEndpointEnabled(req, res, () => {
    res.json({
      'blockchainLength': listBlockchain[id].chain.length
    });
  });
});

//Byzantine fault tolerance
function getMinVotesRequired() {
  return Math.floor(2/3 * networkNodes.length);
}

function getNodeStatus() {
  return {
    note: `${networkNodes.length} network node(s) active`,
    "networkNodes": networkNodes
  };
}

app.get('/blockchain/:page/:id', function(req, res) {
  isEndpointEnabled(req, res, () => {
    const id = req.params.id;
    const page = Number(req.params.page);
    const totalPages = Math.ceil(listBlockchain[id].chain.length / 100);
    const previous = page - 1 >= 0 ? page - 1: -1;
    const next = page + 1 < totalPages ? page + 1: -1;

    const start = page * 100;
    const end = start + 100;

    const response = {
      totalPages: totalPages,
      baseUrl: getURI(nodeIp, '/blockchain/'),
      previousUrl: previous !== -1 ? getURI(nodeIp, `/blockchain/${previous}`) : `none`,
      nextUrl: next !== -1 ? getURI(nodeIp, `/blockchain/${next}`) : `none`,
      chain: listBlockchain[id].chain.slice(start, end)
    }
    res.send(response)
  })
})

app.get('/nodes', function (req, res) {
  isEndpointEnabled(req, res, () => {
    res.json(getNodeStatus())
  })
})

function makeVoteEmissionRequest(networkNodeUrl, newBlockHash, newBlockIndex, vote, id) {
  return makePostRequest(networkNodeUrl, `/receive-vote/${id}`, {
    "newBlockHash": newBlockHash,
    "newBlockIndex": newBlockIndex,
    "vote": vote,
    "nodeAddress": nodeIp
  });
}

app.post('/receive-vote/:id', function (req, res) {
  isEndpointEnabled(req, res, () => {
    const id = req.params.id;
    const blockHash = req.body.newBlockHash;
    const blockIndex = req.body.newBlockIndex;
    const vote = req.body.vote;
    log(`Vote ${vote} by ${req.body.nodeAddress} was received on block ${blockHash}`);
    votingStatistics.voteReceived(vote, req.body.nodeAddress);
    let blockchain = new Blockchain();
    blockchain.updateInstance(listBlockchain[id]);
    const addresults = blockchain.processVote(blockHash, blockIndex, req.body.nodeAddress, vote);
    setTimeout(() => {
    const results = blockchain.getVotes(blockHash, blockIndex);
    if ('warning' in results) {
      log(results.warning);
      res.json({
        note: results.warning
      })
    } else {
          if (results.yesVotes >= getMinVotesRequired()) {
            blockchain.status = 'PREPARED';
            listBlockchain[blockchain.id].status = 'PREPARED';
            log(`Node status updated to PREPARED`);
            const sendCommitPromises = [];
              sendCommitPromises.push(rp(makeCommitEmissionRequest(req.body.nodeAddress, blockHash, blockIndex,id)));
  
            Promise.all(sendCommitPromises)
            .then(function(body) {
              log(`Commit transmission results:\n${JSON.stringify(body)}`);
              res.end();
            })
            .catch(err => {
              log(`error on sending commit request: ${err}`);
            });
          } else if (results.yesVotes < getMinVotesRequired()) {
            const closeConsensusTime = () => {
              votingStatistics.consensusFinished();
              log(`Consensus total time: ${votingStatistics.consensusTotalTime}ms`);
            };
            blockchain.closeVotingOnBlock(blockHash);
            log(`Consensus was NOT reached on prepare, new block (${blockHash}) was discarded`);
            closeConsensusTime();
          }
  
          res.json({
            note: `Vote on block ${req.body.newBlockHash} acknowledged by node ${nodeIp}`
          });
        
    }
  }, 1000);
  });
})

app.post('/validate/:id', function (req, res) {
  isEndpointEnabled(req, res, () => {
    const id = req.params.id;
    try {
      votingStatistics.validationStarted();
    } catch(err) {
      votingStatistics = new VotingStatistics();
      votingStatistics.validationStarted();
    }

    log(`Starting validation on block ${req.body.createdBlock['hash']}`);
    log(`Block received from: ${req.connection.remoteAddress}`);
    let blockchain = new Blockchain();
    blockchain.updateInstance(listBlockchain[id]);
    blockchain.putBlockOnHold(req.body.createdBlock);
    blockchain.status = 'PRE-PREPARED'
    listBlockchain[blockchain.id].status = 'PRE-PREPARED'
    log(`Node status updated to PRE-PREPARED`);
    const newBlockHash = req.body.createdBlock['hash'];
    const newBlockIndex = req.body.createdBlock['index'];
    const validationResult = blockchain.isValidNewBlock(req.body.createdBlock);
    const isValidBlock = validationResult.isValid;

    if(!isValidBlock) {
      log(`block ${newBlockHash} is NOT valid, details as follows:`);
      log(validationResult.details);
    }

    const vote = isValidBlock ? "yes" : "no";

    votingStatistics.localValidationFinished();
    log(`Block validation time: ${votingStatistics.validationLocalTime}ms`);

    //broadcast vote to every node for validation

    const sendVotePromises = [];
    for (let i = 0; i < networkNodes.length; i++) {
      sendVotePromises.push(rp(makeVoteEmissionRequest(networkNodes[i], newBlockHash, newBlockIndex, vote, id)));
    }

    Promise.all(sendVotePromises)
    .then(function(body) {
      log(`Vote transmission results:\n${JSON.stringify(body)}`);
      votingStatistics.validationResultsReceived();
      log(`Total validation time: ${votingStatistics.validationTotalTime}ms`);

      res.json({
        note: `Block ${newBlockHash} processed and vote ${vote} transmitted to the network`,
        "nodeAddress": nodeIp
      });
    })
    .catch(err => {
      log(`error on sending voting request: ${err}`);
      setTimeout(() => {
        const closeConsensusTime = () => {
          votingStatistics.consensusFinished();
          log(`Consensus total time: ${votingStatistics.consensusTotalTime}ms`);
        };
        const results = blockchain.processVote(newBlockHash, newBlockIndex, req.body.nodeAddress, "no");
        if (results.yesVotes < getMinVotesRequired()) {
          console.log(getMinVotesRequired());
          console.log(results.yesVotes);
          blockchain.closeVotingOnBlock(newBlockHash);
          log(`Consensus was NOT reached on pre-prepared, new block (${newBlockHash}) was discarded`);
          closeConsensusTime();
        }
      }, 1000);
    });
  })
})

function makeValidationRequest(networkNodeUrl, body, createdBlock, id) {
  return makePostRequest(networkNodeUrl, `/validate/${id}`, {
    "originalBody": body,
    "createdBlock": createdBlock
  });
}

function makeCommitEmissionRequest(networkNodeUrl, newBlockHash, newBlockIndex, id) {
  return makePostRequest(networkNodeUrl, `/receive-commit/${id}`, {
    "newBlockHash": newBlockHash,
    "newBlockIndex": newBlockIndex,
    "nodeAddress": nodeIp
  });
}

app.post('/receive-commit/:id', function (req, res) {
  isEndpointEnabled(req, res, () => {
    const id = req.params.id;
    const blockHash = req.body.newBlockHash;
    const blockIndex = req.body.newBlockIndex;
    let blockchain = new Blockchain();
    blockchain.updateInstance(listBlockchain[id]);
    const addresults = blockchain.processCommit(blockHash, blockIndex, req.body.nodeAddress);
    console.log(addresults);
    setTimeout(()=>{
      const results = blockchain.getCommits(blockHash, blockIndex);
      if ('warning' in results) {
        log(results.warning);
        res.json({
          note: results.warning
        })
      } else {
        const closeConsensusTime = () => {
          votingStatistics.consensusFinished();
          log(`Consensus total time: ${votingStatistics.consensusTotalTime}ms`);
        };
        if (results.commitMessages >= getMinVotesRequired()) {
          blockchain.addBlockOnBuffer(blockHash);
          if (id === 'database'){
            const chainlength = listBlockchain['database'].chain.length;
            const addquery = jsonSql.build(listBlockchain['database'].chain[chainlength-1].data.data).query;
            const blockquery = listBlockchain['database'].chain
            fillDatabase(blockquery);
            db.query(addquery,function(error,results, fields) {
              if (error){
                console.log("ERROR", error.sqlMessage)
              };
              console.log('query result: ', results)
            });
          }
          blockchain.status = 'COMMITED'
          listBlockchain[blockchain.id] = blockchain
          isCreateBlockSuccess = true;
          log(`Node status updated to COMMITED`);
          log(`Consensus was reached, new block ${blockHash} added to the blockchain`);
          closeConsensusTime();
        } else if (results.commitMessages >= networkNodes.length) {
          blockchain.closeVotingOnBlock(blockHash);
          log(`Consensus was NOT reached on commit, new block (${blockHash}) was discarded`);
          closeConsensusTime();
        }
  
        res.json({
          note: `Commit on block ${req.body.newBlockHash} acknowledged by node ${nodeIp}`
        });
      }
    }, 1000)
    
    
  })
})

function checkTimestampFormat(timestamp) {
  return (!!timestamp.match(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/));
}

function getCurrentTimestamp() {
  return (new Date()).toISOString().replace("T", " ").replace("Z", "");
}

app.post('/createBlock/:id', function(req, res) {
  isEndpointEnabled(req, res, () => {
    const id = req.params.id
    log(`Received request to create block from ${req.connection.remoteAddress}`);
    votingStatistics = new VotingStatistics();
    votingStatistics.validationStarted();
    
    log(`Creating block and broadcast to network`);
    log(JSON.stringify(req.body));

    let timestamp;
    if ('timestamp' in req.body && checkTimestampFormat(req.body['timestamp'])) {
      timestamp = req.body['timestamp'];
    } else {
      timestamp = getCurrentTimestamp();
    }

    log(`Timestamp used: ${timestamp}`);
    console.log(id);
    let blockchain = new Blockchain();
    blockchain.updateInstance(listBlockchain[id]);
    const createdBlock = blockchain.createBlock(blockchain.getLastBlock()['hash'], req.body.block, timestamp);
    
    votingStatistics.blockCreationLocalFinished();
    log(`Block creation time: ${votingStatistics.blockCreationLocalTime}ms`);

    // broadcast block to every node for validation
    const validateNodesPromises = [];
    for(let i = 0; i < networkNodes.length; i++) {
      validateNodesPromises.push(rp(makeValidationRequest(networkNodes[i], req.body, createdBlock, id)));
    }
    // self validate
    validateNodesPromises.push(rp(makeValidationRequest(nodeIp, req.body, createdBlock, id)));

    Promise.all(validateNodesPromises)
    .then((body) => { //result of each request
      log(`Block insertion results:\n${JSON.stringify(body)}`);
      votingStatistics.blockCreationResultsReceived();
      log(`Create blok total time: ${votingStatistics.blockCreationTotalTime}ms`);
      setTimeout(()=>{
        if (isCreateBlockSuccess) {
          console.log('here');
          res.json({
            note: `Block ${createdBlock['hash']} created and transmitted to the network for validation`,
            block: createdBlock,
            votingStatistics: votingStatistics.getResults(networkNodes.length)
          })  
        } else {
          res.status(500).send('Error pada pembuatan blockchain')
        }
      }, 2000)
    })
    .catch(err => {
      log(`error on sending validation request: ${err}`);
    });
  })
  // res.end();
});

function makeRegisterRequest(networkNodeUrl, reqAddress) {
  return makePostRequest(networkNodeUrl, "/register-node", {
    nodeAddress: reqAddress,
  });
}

function isValidRegisterRequest(reqAddress) {
  const nodeNotAlreadyPresent = (networkNodes.indexOf(reqAddress) == -1)
  const notCurrentNode = nodeIp != reqAddress;
  //TODO tambah port
  return (nodeNotAlreadyPresent && notCurrentNode && reqAddress);
}

app.post('/register-node', function(req, res) {
  isEndpointEnabled(req, res, () => {
    log(`Received register request from ${req.connection.remoteAddress}`);
    const reqAddress = req.body.nodeAddress;

    if (!isValidRegisterRequest(reqAddress)) {
      log(`Register request from ${reqAddress} is invalid`);
      res.json({
        note: `Invalid request for registering node`
      });
    } else {
      networkNodes.push(reqAddress);
      log(`Node ${reqAddress} added to the list`);
      res.json({
        note: `Node registered successfully on the node ${nodeUuid}, ${nodeIp}`
      });
    }
  })
});

app.post('/register-and-broadcast-node', function(req, res) {
  isEndpointEnabled(req, res, () => {
    log(`Received request from ${req.connection.remoteAddress} to join network`);
    log(JSON.stringify(req.body));
    const reqAddress = req.body.nodeIp;
    if (networkNodes.length < 1) {
      if (!isValidRegisterRequest(reqAddress)) {
        log(`Register request from ${reqAddress} is invalid`);
        res.json({
          note: `Invalid request for registering node`
        });
      } else {
        networkNodes.push(reqAddress);
        log(`Node ${reqAddress} added to the list`);
        let sendnode = []
        sendnode.push(nodeIp)
        res.json({
          networkNodes: sendnode
        });
      }
  } else {

  
    const regNodesPromises = [];
  
    for (var i = 0; i < networkNodes.length; i++) {
      regNodesPromises.push(rp(makeRegisterRequest(networkNodes[i], reqAddress)));
    }

    log(`Broadcasting node ${reqAddress} to network`);
    Promise
    .all(regNodesPromises)
    .then(() => {
      networkNodes.push(reqAddress);
      let sendnetworkNodes = [];
      networkNodes.forEach(node => {
        sendnetworkNodes.push(node);
      });
      sendnetworkNodes.push(nodeIp);
      log(`Node ${reqAddress} added to network`);
      res.json({networkNodes: sendnetworkNodes});
    }).catch((err) => {
      log(`Error broadcasting new node to network`);
      log(err);
      networkNodes.push(reqAddress);
      let sendnetworkNodes = [];
      networkNodes.forEach(node => {
        sendnetworkNodes.push(node);
      });
      sendnetworkNodes.push(nodeIp);
      log(`Node ${reqAddress} added to network`);
      res.json({networkNodes: sendnetworkNodes});
    })
  }
  })
});

app.post('/start-register', function(req, res) {
  isEndpointEnabled(req, res, () => {
    log(`Starting node registration for ${req.connection.remoteAddress}`);
    res.json({
      data: getURI(nodeIp, "/listblockchain")
    })
  })
});

function requestRegister(ip, nodeIp) {
  request.post({
    url: getURI(ip, '/register-and-broadcast-node'),
    form: { nodeIp }
  }, function (err, res, body) {
    log(`Received response for register request`);
    log(body);
    body = JSON.parse(body);
    if (networkNodes.length < 1) {
      if (body['networkNodes'].length > 1) {
        let tempnetworkNodes = body['networkNodes'];
        tempnetworkNodes.forEach(reqAddress => {
        if (isValidRegisterRequest(reqAddress)) {
          networkNodes.push(reqAddress);
        }
        });
      } else {
        networkNodes.push(body['networkNodes'][0]);
      }
    } else {
      tempnetworkNodes = body['networkNodes'];
      tempnetworkNodes.forEach(reqAddress => {
       if (isValidRegisterRequest(reqAddress)) {
         networkNodes.push(reqAddress);
       }
      });
    }
    isBlockchainAvailable = true;
  })
}

app.post('/initconnection/', function(req, res) {
  const nodeAddress = req.body.nodeAddress;
  request.post({
    url: getURI(nodeAddress, "/start-register")
  }, function (err, res, body) {
    log(`Response received, adding network nodes`);
    log(body);
    try{
      body = JSON.parse(body);
    } catch {
      body = false
    }
    if (body) {
      fullUpdateBlockchain(body['data'], () => {
        requestRegister(nodeAddress, nodeIp);
      })
    } else {
      log(`Error connection undefined`)
    }
  })
  res.end();
})

function fullUpdateBlockchain(url, callback) {
  request(url, function(err, res, body) {
    body = JSON.parse(body);
    if ((Object.keys(body).length > Object.keys(listBlockchain).length)){
      //copy blockchain to database
      listBlockchain = {}
      listBlockchain = body;
      const blockquery = listBlockchain['database'].chain
      fillDatabase(blockquery);
    }
      callback();
  })
}

async function fillDatabase (queries) {
  for (let i = 0; i < queries.length; i++){
    console.log(i);
    let query = ''
    if (i === 0) {
      query = queries.find(block => block.index ==i).data.data;
      // console.log(query);
    } else {
      query = jsonSql.build(queries.find(block => block.index ==i).data.data).query;
      console.log(query)
    }
    try {
      const queryresult = await db.query(query);
      console.log(queryresult);
    } catch(err) {
      console.log(err);
    }
  }
}

app.listen(PORT, function () {
  log(`Listening on port ${PORT}...`);
})